var BryApiModule = (function () {
    var BryApiModule = {};

    /**
    * Variável Global que facilita o acesso aos certificados.
    * A função que lista os certificados preenche esta variável.
    * Dessa forma, será possível pegar os certificados desta variável sempre que precisar.
    */
    BryApiModule.certificates = [];

    /**
     * Sempre que a página é carregada é solicitado à extensão que busque
     * todos os certificados do usuário.
     */
    document.addEventListener("DOMContentLoaded", function () {
        if ( BryApiModule.isExtensionInstalled() ) {
            $("#extensao-instalada").show();
            $("#extensao-nao-instalada").hide();

            BryApiModule.listCertificates();
            BryApiModule.isNativeModuleUpdated();

        } else {
            $("#extensao-instalada").hide();
            $("#extensao-nao-instalada").show();
            detectBrowser();
        }
    });

    /**
     * Verifica se a extensão está instalada.
     * @return true para instalada, false para não instalada.
     */
    BryApiModule.isExtensionInstalled = function () {
        if ( typeof BryExtension !== "undefined" && typeof BryExtension.listCertificates === "function" )
            return true;
        else
            return false;
    }

    /**
    * Verifica se o módulo nativo está atualizado.
    * Caso não estiver atualizado irá exibir um botão na página.
    */
    BryApiModule.isNativeModuleUpdated = function () {
        // Função da extensão
        BryExtension.isNativeModuleUpdated()
        .then(function(isUpdated){
            if (isUpdated) {
                $("#update_windows").hide();
                $("#update_linux").hide();
                $("#success-message").hide();
            } else {
                if (navigator.platform === "Win32" || navigator.platform === "Win64") {
                    $("#update_windows").show();
                    $("#update_linux").hide();
                } else {
                    $("#update_linux").show();
                    $("#update_windows").hide();
                }
                $("#success-message").hide();
            }
        })
        .catch(function(error){
            // Do Nothing!
        });
    }

    /**
    * Função utilizada para listar os certificados na máquina do usuário.
    * Essa função redireciona a chamada para a extensão e configura o campo de certificados.
    */
    BryApiModule.listCertificates = function () {
        // Função da extensão
        BryExtension.listCertificates()
        .then(function(certificates){
            BryApiModule.updateCertificates(certificates);
        })
        .catch(function(error){
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        });
    }
	
    /**
    * Função utilizada para atualizar o combo de certificados. Antes de popular o combo esta função
    * aplica um conjunto de filtros.
    *
    * @param {Array} certificates - Detalhes sobre os campos do certificado estão na documentação.
    */
    BryApiModule.updateCertificates = function (certificates) {
        BryApiModule.certificates = BryApiModule.filter(certificates);
        BryApiModule.fillCertificateSelect(BryApiModule.certificates);
    }
	
    /**
     * Essa função é responsável por popular o elemento "select" na página
     * que o usuário utiliza para indicar qual o certificado deseja utilizar
     * para produzir a assinatura digital.
     *
     * Será utilizado as propriedades "nome" e "certId" do certificado. Posteriormente,
     * será possível pegar o "certId" do certificado selecionado e consultar a variável
     * global de certificados para pegar os bytes do certificado.
     *
     * @param {Array} certificates
     */
    BryApiModule.fillCertificateSelect = function (certificates) {
        var element = document.getElementById("select-certificado-list");
        element.innerHTML = "";

        var hasCertificateAvailable = certificates.length > 0;
        if (hasCertificateAvailable) {
            for (var i = 0; i < certificates.length; i++) {
                var certificate = certificates[i];
                var option = document.createElement("option");
                option.value = certificate.certId;
                option.innerHTML = certificate.name;
                element.appendChild(option);
            }
        } else {
            var option = document.createElement("option");
            option.text = "Nenhum certificado disponivel";
            element.add(option);
        }

        BryApiModule.fillCertificateDataForm();
    }

    /**
    * Realiza o processo de assinatura do input de entrada.
    *
    * É necessário informar o "certId" do certificado e os dados de entrada.
    * Para entender os dados de entrada do processo de assinatura favor consultar a documentação.
    */
    BryApiModule.sign = function () {
        // Limpa o json de saída, setando "" string vazia a cada requisição.
        document.getElementById("json-saida-valor").innerHTML = "";

        var input = $("#json-entrada-valor").text();
        var element = document.getElementById("select-certificado-list");
        var idSelectedCertificate = element.value;

        // Função da extensão
        BryExtension.sign(idSelectedCertificate, input)
            .then(function(response){
                BryApiModule.processSignatures(response);
            })
            .catch(function(error){
                $("#codigo-de-erro").html(error.key);
                $("#error-message-text").text(error.description);
                $("#error-message").show();
                $("#success-message").hide();
                $("#update").hide();
            });
    }

    /**
     * Essa função é chamada sempre que a página recebe devolta os
     * dados processados da assinatura. Esses dados precisam ser devolvidos
     * ao servidor para que a assinatura seja completada. Os dados
     * produzidos pela extensão são colocados em um "input" e então
     * o servidor é notificado que esses dados estão prontos.
     *
     * @param {Object} data - dados produzidos pela extensão.
     * Consulte a documentação do desenvolvedor.
     */
    BryApiModule.processSignatures = function (data) {
        document.getElementById("json-saida-valor").innerHTML = JSON.stringify(data);
        $("#success-message").show();
        $("#error-message").hide();
        $("#update").hide();
    }

    /**
    * Realiza o processo de assinatura do input de entrada.
    *
    * É necessário informar o "certId" do certificado e os dados de entrada.
    * Para entender os dados de entrada do processo de assinatura favor consultar a documentação.
    */
    BryApiModule.signChallenge = function () {
        // Limpa o json de saída, setando "" string vazia a cada requisição.
        document.getElementById("json-saida-valor").innerHTML = "";

        var challenge = $("#json-entrada-valor").text();

        var input = BryApiModule.prepareChallengeForSignature(challenge);
        var element = document.getElementById("select-certificado-list");
        var idSelectedCertificate = element.value;

        // Função da extensão
        BryExtension.sign(idSelectedCertificate, input)
            .then(function(response){
                BryApiModule.processSignatures(response);
            })
            .catch(function(error){
                $("#codigo-de-erro").html(error.key);
                $("#error-message-text").text(error.description);
                $("#error-message").show();
                $("#success-message").hide();
                $("#update").hide();
            });
    }

    /**
    * Função auxiliar para formatar os desafios que devem ser assinados.
    * Esta função formata apenas um desafio, pode-se utilizar um array caso desejado.
    */
    BryApiModule.prepareChallengeForSignature = function (challenge) {
        // A configuração do nonce é opcional.
        // O nonce_lote e o nonce_desafios podem ser omitidos da mensagem.
        // Gere o nonce da forma que sua equipe julgar ideal.
        var nonceBatch = "123456789";
        var nonceChallenges = "32056362762941";

        // O algoritmo hash é obrigatório
        var hashAlgorithm = "SHA256";

        var input = {
            "algoritmoHash": hashAlgorithm,
            "nonce": nonceBatch,
            "formatoDadosEntrada": "Hexadecimal",
            "formatoDadosSaida": "Hexadecimal",
            "assinaturas": [
                {
                    "nonce": nonceChallenges,
                    "hashes": [ challenge ]
                }
            ]
        };

        return JSON.stringify(input);
    }

    /**
    * Variável Global que facilita o acesso às leitoras.
    * A função que lista as leitoras preenche esta variável.
    * Dessa forma, será possível pegar as leitoras desta variável sempre que precisar.
    */
    BryApiModule.readers = [];
	
    BryApiModule.listReaders = function () {
        // Função da extensão
        BryExtension.listReaders()
        .then(function(readers){
            BryApiModule.updateReaders(readers);
        })
        .catch(function(error){
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        });
    }

    BryApiModule.updateReaders = function (readers) {
        BryApiModule.readers = readers;
        BryApiModule.fillReadersSelect(BryApiModule.readers);
    }

    BryApiModule.fillReadersSelect = function (readers) {
        var element = document.getElementById("select-reader-list");
        element.innerHTML = "";

        if(readers.length == 0){
            var option = document.createElement("option");
            option.text = "Nenhuma leitora disponível";
            element.add(option);
        }
		else {
			for (var i = 0; i < readers.length; i++) {
				var reader = readers[i];
				var option = document.createElement("option");
				option.value = reader.name;
				option.innerHTML = reader.name+" | "+reader.device;
				element.appendChild(option);
			}
		}
    }

    BryApiModule.certificateRequestGenerate = function () {		
		// Limpa o json de saída, setando "" string vazia a cada requisição.
        document.getElementById("json-saida-valor").innerHTML = "";

		var input = $("#json-entrada-valor").val();
		//Caso deseje gerar a requisição em um token/smartcard; Para gerar no repositório local do windows, basta manter o valor do campo location como "DEFAULT".
		if(document.getElementById("optradio-reader").checked) {
			var inputJSON = JSON.parse(input);
			//Muda o campo location para TOKEN.
			inputJSON.key.location = "TOKEN";
			//Popula o campo key->reader do JSON com o objeto da leitora selecionada. O mesmo objeto que é retornado na função listReaders pode ser utilizado.
			inputJSON.key.reader = BryApiModule.readers[document.getElementById("select-reader-list").selectedIndex];
			input = JSON.stringify(inputJSON);
        }

		$("#pleaseWaitDialog").modal("show");
        //Função da extensão
        BryExtension.certificateRequestGenerate(input)
        .then(function(response){
            $("#pleaseWaitDialog").modal("hide");
            BryApiModule.processSignatures(response);
        })
        .catch(function(error){
            $("#pleaseWaitDialog").modal("hide");
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        });
    }
	
    BryApiModule.certificateRequestImport = function () {		
		//Recupera o JSON de saída de requisição gerada.
		var outputValue = document.getElementById("json-saida-valor").innerHTML;
		var JSONSaidaRequisicao = JSON.parse(outputValue);
		// Limpa o json de saída, setando "" string vazia a cada requisição.
        document.getElementById("json-saida-valor").innerHTML = "";
		
		//Apenas para realização de testes, uma requisição de importação já está configurada com um certificado para ser importado.
		//Este certificado deve ser substituido pelo gerado a partir da requisição.
        var input = $("#json-entrada-valor-import").val();	
		
		var inputJSON = JSON.parse(input);
		//Substitui o campo keyInfo da importação, pelo campo keyInfo retornado na requisição gerada. Se na requisição foi solicitado que a chave fosse gerada no token, irá retornar o keyInfo do token.
		inputJSON.keyInfo = JSONSaidaRequisicao.keyInfo;
		
		input = JSON.stringify(inputJSON);
	
        // Função da extensão
        BryExtension.certificateRequestImport(input)
        .then(function(response){
            BryApiModule.processSignatures(response);
        })
        .catch(function(error){
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        });
    }
	
    BryApiModule.exportCertificate = function () {		
		// Limpa o json de saída, setando "" string vazia a cada requisição.
        document.getElementById("json-saida-valor").innerHTML = "";
		var input = $("#json-entrada-valor-export").val();
        // Função da extensão
        BryExtension.exportCertificate(input)
        .then(function(response){
            BryApiModule.processSignatures(response);
        })
        .catch(function(error){
            $("#pleaseWaitDialog").modal("hide");
            $("#codigo-de-erro").html(error.key);
            $("#error-message-text").text(error.description);
            $("#error-message").show();
            $("#success-message").hide();
            $("#update").hide();
        });
    }

    /**
     * Essa função é chamada sempre que o usuário altera sua opção de
     * qual certificado deseja utilizar.
     * Consulta a variável global de certificados para pegar informações do
     * certificado selecionado.
     */
    BryApiModule.fillCertificateDataForm = function () {
        var element = document.getElementById("select-certificado-list");
        var selected = element.value;

        var certificate = null;
        for (var i = 0; i < BryApiModule.certificates.length; i++) {
            if (BryApiModule.certificates[i].certId === selected) {
                certificate = BryApiModule.certificates[i];

                $("#input-nome").val(certificate.name);
                $("#input-emissor").val(certificate.issuer);
                $("#input-data-validade").val(certificate.expirationDate);
                $("#input-tipo").val(certificate.certificateType);
                break;
            }
        }
    }

    /**
    * Configurações utilizadas no momento que os filtros são aplicados.
    */
    BryApiModule.filters = {
        ROOT_CA: 0, // 0=Todos os certificados, 1=Somente Confiáveis, 2=Somente ICP-Brasil
        CNPJS: [], // ex.: ["CNPJ1","CNPJ2"]
        CPFS: [], // ex.: ["CPF","CPF2"]
        CERTIFICATE_TYPE: [], // ex.: ["A1","A2","A3"]
        SHOW_EXPIRED: true
    }

    /**
    * Função auxiliar que permite recuperar os dados dos parâmetros configurados na página, aqueles antigos ex.:
    * <input class="bryws_parametro" type="hidden" name="id_combo_certificados" value="..."/>
    * Utilize esta função apenas se quiser que a aplicação recupera os campos configurados na página, devido a migração da versão WebSocket.
    */
    BryApiModule.backwardsCompatibility = function () {
        if( document.getElementsByName("certificado_filtro_ac_raiz").length > 0 )
            BryApiModule.filters.ROOT_CA = document.getElementsByName("certificado_filtro_ac_raiz")[0].value;

        if( document.getElementsByName("certificado_filtro_cnpj").length > 0 )
        {
            var aux = document.getElementsByName("certificado_filtro_cnpj")[0].value.split(',');
            if(!(aux.length == 1 && aux[0] === ""))
                BryApiModule.filters.CNPJS = aux;
        }

        if( document.getElementsByName("certificado_filtro_cpf").length > 0 )
        {
            var aux = document.getElementsByName("certificado_filtro_cpf")[0].value.split(',');
            if(!(aux.length == 1 && aux[0] === ""))
                BryApiModule.filters.CPFS = aux;
        }

        if( document.getElementsByName("certificado_filtro_tipo").length > 0 )
        {
            var aux = document.getElementsByName("certificado_filtro_tipo")[0].value.split(',');
            if(!(aux.length == 1 && aux[0] === ""))
                BryApiModule.filters.CERTIFICATE_TYPE = aux;
        }

        if( document.getElementsByName("certificado_filtrar_invalidos").length > 0 )
            BryApiModule.filters.SHOW_EXPIRED = document.getElementsByName("certificado_filtrar_invalidos")[0].value;
    }

    /**
     * Função responsável pela aplicação dos filtros. Essa função aplica
     * uma série de filtros aos certificados recebidos. Esses filtros são
     * aplicados um após o outro e apenas os certificados que satisfizerem
     * todos os filtros são retornados.
     *
     * @param {Array} certificates - certificados que serão filtrados.
     * @returns {Array} os certificados que satisfizeram todos os filtros.
     * Consulte a documentação do desenvolvedor.
     */
    BryApiModule.filter = function (certificates) {
        let filtered = BryApiModule.filterByCpf(certificates);
        filtered = BryApiModule.filterByCnpj(filtered);
        filtered = BryApiModule.filterByType(filtered);
        filtered = BryApiModule.filterExpired(filtered);
        filtered = BryApiModule.filterByRootCA(filtered);
        return filtered;
    }

    /**
     * Filtra os certificados através de uma lista de CPFs.
     *
     * @param {Array} certificates - Os certificados que devem ser filtrados
     * Consulte a documentação do desenvolvedor.
     */
    BryApiModule.filterByCpf = function (certificates) {
        var cpfs = BryApiModule.filters.CPFS;
        if( cpfs.length > 0 )
            return certificates.filter(function (certificate){
                return cpfs.find(function (cpf) {
                    return onlyNumbers(cpf) === onlyNumbers(certificate.cpf);
                });
            });
        else
            return certificates;
    }

    /**
     * Filtra os certificados através de uma lista de CNPJs.
     *
     * @param {Array} certificates - Os certificados que devem ser filtrados
     * Consulte a documentação do desenvolvedor.
     */
    BryApiModule.filterByCnpj = function (certificates) {
        var cnpjs = BryApiModule.filters.CNPJS;
        if( cnpjs.length > 0 )
            return certificates.filter(function (certificate){
                return cnpjs.find(function (cnpj) {
                    return onlyNumbers(cnpj) === onlyNumbers(certificate.cnpj);
                })
            });
        else
            return certificates;
    }

    /**
     * Filtro baseado no tipo do certificado.
     *
     * @param {Array} certificates - Os certificados que devem ser filtrados
     */
     BryApiModule.filterByType = function (certificates) {
         var types = BryApiModule.filters.CERTIFICATE_TYPE;
         if( types.length > 0 )
             return certificates.filter(function (certificate) {
                 return types.find(function (type) {
                     return type === certificate.certificateType;
                 })
             });
         else
             return certificates;
     }

    /** Habilita/desabilita a exibição de certificados expirados.
     * @param {Array} certificates - Os certificados que devem ser filtrados
    */
    BryApiModule.filterExpired = function (certificates) {
        if (BryApiModule.filters.SHOW_EXPIRED === false) {
            for (var i = certificates.length - 1; i >= 0; --i) {
                if (certificates[i].validity.localeCompare("VALID") != 0 && certificates[i].validity.localeCompare("INVALID_INCOMPLET_CHAIN") != 0 )
                    certificates.splice(i, 1);
            }
        }
        return certificates;
    }

    /** Habilita/desabilita a exibição de certificados expirados.
     * @param {Array} certificates - Os certificados que devem ser filtrados
    */
    BryApiModule.filterByRootCA = function (certificates) {
        //0 é o comportamento padrão, ou seja, sem filtro
        var selectedValue = BryApiModule.filters.ROOT_CA;

        //Remove os certificados que não são confiáveis
        if (selectedValue == 1) {
            for (var i = certificates.length - 1; i >= 0; --i) {
                if (!certificates[i].trusted) certificates.splice(i, 1);
            }
        //Remove os certificados que não são ICP-Brasil
        } else if (selectedValue == 2) {
            for (var i = certificates.length - 1; i >= 0; --i) {
                if (!certificates[i].icpBrasil) certificates.splice(i, 1);
            }
        }

        return certificates;
    }

    /**
     * Remove todos os dígitos não-numéricos do campo informado
     * @param {any} field campo com o texto que deverá ser tratado
     * @returns {string} string contendo apenas os digitos númericos
     */
    function onlyNumbers(field) {
        return field.replace(/^\D+/g, '');
    }

    /**
     * Transforma a string de data retornada pela extensão em um objeto Date do javascript.
     * @param {any} date O formato esperado dessa string é "dd/MM/yyyy HH:mm:ss".
     * @returns {Date} objeto Date configurado com a data transformada da string.
     */
    function parseDate(date) {
        var slicedDate = date.split(" ");
        var dayMothYear = slicedDate[0].split("/");
        var hourMinuteSecond = slicedDate[1].split(":");
        return new Date(dayMothYear[2], dayMothYear[1] - 1, dayMothYear[0],
            hourMinuteSecond[0], hourMinuteSecond[1], hourMinuteSecond[2]);
    }

    /**
     * Funções simplificadas para detecção do browser, modifique se achar necessário
     */
    function isOpera() {
        return !!window.opera || navigator.userAgent.indexOf(' OPR/') >= 0;
    }

    function isFirefox() {
        return typeof InstallTrigger !== 'undefined';
    }

    function isSafari() {
        return navigator.userAgent.indexOf("Safari") > -1
    }

    function isIE() {
        return /*@cc_on!@*/ false || !!document.documentMode;
    }

    function isEdge() {
        if (document.documentMode || /Edge/.test(navigator.userAgent)) {
			return true;
		}
		else {
			return false;
		}
    }

    function isChrome(){
        return !!window.chrome && !!window.chrome.webstore && !isOpera();
    }

    function detectBrowser() {
        $("#chrome-browser").hide();
        $("#firefox-browser").hide();
        $("#opera-browser").hide();
        $("#edge-browser").hide();
        $("#safari-browser").hide();
        $("#ie-browser").hide();
        $("#unknown-browser").hide();
        if(isChrome()){
            $("#chrome-browser").show();
        }
        else if (isFirefox())
        {
            $("#firefox-browser").show();
        }
        else if (isEdge())
        {
            $("#edge-browser").show();
        }
        else if (isOpera())
        {
            $("#opera-browser").show();
        }
        else if (isSafari())
        {
            $("#safari-browser").show();
        }
        else if (isIE())
        {
            $("#ie-browser").hide();
        }
        else
        {
            $("#unknown-browser").hide();
        }
    }

	/**
	* ATENÇÃO: A instalação inline funciona apenas no domínio do criador da extensão.
	* ex.: Se o criador for bry.com.br, funcionará apenas neste domínio.
	*
	* Para configurar o inline, favor adicionar o id da extensão no header da página antes de chamar esta função
	* <link rel="chrome-webstore-item" href="https://chrome.google.com/webstore/detail/dhikfimimcjpoaliefjlffaebdeomeni">
	*
	* Instalação não inline é aquela que direciona para o webstore.
	* No firefox a instalação sempre será inline porque utilizando um link direto para o xpi da extensão.
	*/
    BryApiModule.installExtensionInline = function () {
        if(isChrome())
        {
            chrome.webstore.install('https://chrome.google.com/webstore/detail/dhikfimimcjpoaliefjlffaebdeomeni',
                function(){window.location.reload();},
                function(reason){}
            );
        }
		else if(isSafari()) {
			window.open("https://itunes.apple.com/br/app/bry-assinatura-digital/id1315721873?mt=12", "_blank");
		}
        else
        {
            promptDownload("https://www.bry.com.br/downloads/extension/firefox/assinatura_digital_bry.xpi");
            function timeout() {
                setTimeout(function () {
                   if(BryApiModule.isExtensionInstalled()){
                    window.location.reload();
                   }
                    timeout();
                }, 1000);
            }
            timeout();
        }
		
    }
	
    BryApiModule.redirectToInstallExtension = function () {
        if(isChrome())
		{
			window.addEventListener('focus', () => window.location.reload());
            window.open("https://chrome.google.com/webstore/detail/dhikfimimcjpoaliefjlffaebdeomeni");
		}
        else
		{
            promptDownload("https://www.bry.com.br/downloads/extension/firefox/assinatura_digital_bry.xpi");

			// Opcional, após instalar a extensão detecta se foi instalada para fazer reload da página.
			function timeout() {
                setTimeout(function () {
                   if(BryApiModule.isExtensionInstalled()){
                    window.location.reload();
                   }
                    timeout();
                }, 1000);
            }
            timeout();
		}
    }

	/**
	* Essa função faz a chamada para download do módulo nativo do Windows.
	* Observe a página de exemplo html que direciona o botão de download da atualização para esta função.
	*/
    BryApiModule.downloadNativeModuleWindows = function () {
	    BryExtension.getNativeModuleUrl("windows").then(function (url) {
			promptDownload(url);
	    })
	    .catch(function (error) {
		$("#codigo-de-erro").html(error.key);
		$("#error-message-text").text(error.description);
		$("#error-message").show();
		$("#success-message").hide();
		$("#update").hide();
	    });
    }

	/**
	* Essa função faz a chamada para download do modulo nativo no Linux, distribuições baseadas em Debian (Ubuntu, Mint).
	* A função verifica a arquitetura do sistema operacional e faz a chamada para 32 ou 64 bits.
	* Observe a página de exemplo html que direciona o botão de download da atualização para esta função.
	*/
    BryApiModule.downloadNativeModuleDeb = function () {
    	if (navigator.platform.search("x86_64") > -1) {
    		BryExtension.getNativeModuleUrl("linux_deb_amd64").then(function (url) {
				promptDownload(url);
		    })
		    .catch(function (error) {
				$("#codigo-de-erro").html(error.key);
				$("#error-message-text").text(error.description);
				$("#error-message").show();
				$("#success-message").hide();
				$("#update").hide();
		    });
    	} else {
    		BryExtension.getNativeModuleUrl("linux_deb_i686").then(function (url) {
				promptDownload(url);
		    })
		    .catch(function (error) {
				$("#codigo-de-erro").html(error.key);
				$("#error-message-text").text(error.description);
				$("#error-message").show();
				$("#success-message").hide();
				$("#update").hide();
		    });
    	}
    }

	/**
	* Essa função faz a chamada para download do modulo nativo no Linux, distribuições baseadas em Redhat (Fedora, OpenSuse).
	* A função verifica a arquitetura do sistema operacional e faz a chamada para 32 ou 64 bits.
	* Observe a página de exemplo html que direciona o botão de download da atualização para esta função.
	*/
    BryApiModule.downloadNativeModuleRpm = function () {
    	if (navigator.platform.search("x86_64") > -1) {
    		BryExtension.getNativeModuleUrl("linux_rpm_amd64").then(function (url) {
				promptDownload(url);
		    })
		    .catch(function (error) {
				$("#codigo-de-erro").html(error.key);
				$("#error-message-text").text(error.description);
				$("#error-message").show();
				$("#success-message").hide();
				$("#update").hide();
		    });
    	} else {
    		BryExtension.getNativeModuleUrl("linux_rpm_i686").then(function (url) {
				promptDownload(url);
		    })
		    .catch(function (error) {
				$("#codigo-de-erro").html(error.key);
				$("#error-message-text").text(error.description);
				$("#error-message").show();
				$("#success-message").hide();
				$("#update").hide();
		    });
    	}
    }

    /**
     * Faz o download do arquivo indicado na url
     *
     * @param {string} url url do arquivo que deve ser baixado.
     */
    function promptDownload(url) {
        var elementDownload = document.createElement("a");
        elementDownload.setAttribute('href', url);

        elementDownload.style.display = 'none';
        document.body.appendChild(elementDownload);

        elementDownload.click();

        document.body.removeChild(elementDownload);
    }

    return BryApiModule;
})();
